create table member
(
  id serial PRIMARY KEY,
  name varchar(64) UNIQUE NOT NULL,
  phone varchar(16) UNIQUE NOT NULL,
  level int NOT NULL
);

insert member ( name, phone, level ) values ( 'Michał Pletty' );

create table trening
(
  id serial PRIMARY KEY,
  datetime varchar(64) UNIQUE NOT NULL,
  confirmed boolean NOT NULL
);

create table declaration
(
  member_id serial,
  trening_id serial,
  FOREIGN KEY (member_id) REFERENCES member (id) ON DELETE CASCADE,
  FOREIGN KEY (trening_id) REFERENCES trening (id) ON DELETE CASCADE
);

alter table declaration add unique(member_id, trening_id);

select m.name, t.datetime from member as m
  inner join declaration as d on m.id = d.member_id
  inner join trening as t on t.id = d.trening_id;

select m.name from member as m
  inner join declaration as d on m.id = d.member_id
  inner join trening as t on t.id = d.trening_id;
  where t.datetime = ''
