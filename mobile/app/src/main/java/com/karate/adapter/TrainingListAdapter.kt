package com.karate.adapter

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import android.widget.TextView
import android.widget.ToggleButton
import androidx.fragment.app.FragmentActivity
import com.karate.R
import com.karate.Storage
import okhttp3.Response
import org.json.JSONArray

class TrainingListAdapter(
  activity: FragmentActivity?,
  context: Context,
  private val training_item_view: Int,
  private val training_list: List<String>,
  private val declared_training_list: List<String?>,
  private val phone: String
) :
  ArrayAdapter<String?>(context, training_item_view, training_list)
{
  private val some_declared_alert_builder: AlertDialog.Builder
  private val no_declared_alert_builder: AlertDialog

  init
  {
    some_declared_alert_builder = AlertDialog.Builder(activity)
      .setTitle("uczestnicy")
      .setPositiveButton("ok") { dialog, _ -> dialog.cancel() }
    no_declared_alert_builder = AlertDialog.Builder(activity)
      .setTitle("brak uczestników")
      .setPositiveButton("ok") { dialog, _ -> dialog.cancel() }
      .create()
  }

  override fun getView(
    position: Int,
    _convertView: View?,
    parent: ViewGroup
  ): View
  {
    var convertView = _convertView
    if (convertView == null)
    {
      convertView = LayoutInflater.from(context).inflate(training_item_view, parent, false)
    }
    val datetime = training_list[position]
    val textView = convertView!!.findViewById<TextView>(R.id.datetime)
    textView.text = datetime

    val toggleButton = convertView.findViewById<ToggleButton>(R.id.training_declaration)
    toggleButton?.isChecked = declared_training_list.contains(datetime)

    textView.setOnClickListener { view: View? ->
      val callback = object : Storage.AsyncCallback
      {
        override fun success(response: Response)
        {
          val adapter = ArrayAdapter<String>(context, android.R.layout.simple_list_item_1)

          val responseString = response.body!!.string()
          val jsonArray = JSONArray(responseString)

          for (i in 0 until jsonArray.length())
          {
            adapter.add(jsonArray.getString(i))
          }
          view?.post {
            some_declared_alert_builder
              .setAdapter(adapter) { _, _ -> }
              .show()
          }
        }

        override fun failure()
        {
          view?.post { no_declared_alert_builder.show() }
        }
      }
      Storage.list_declared(datetime, callback)
    }

    toggleButton?.setOnCheckedChangeListener { buttonView: CompoundButton?, isChecked: Boolean ->
      if (isChecked)
      {
        Storage.declare_attendance(phone, datetime)
      }
      else
      {
        Storage.undeclare_attendance(phone, datetime)
      }
    }

    return convertView
  }
}