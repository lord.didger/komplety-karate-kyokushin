package com.karate.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.karate.R
import com.karate.Storage
import com.karate.adapter.TrainingListAdapter
import okhttp3.Response
import org.json.JSONArray
import java.util.*

class ListTrainingsFragment : Fragment()
{
  private val training_list: MutableList<String> = ArrayList()
  private val declared_training_list: MutableList<String> = ArrayList()
  private var trainingListAdapter: TrainingListAdapter? = null

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View
  {
    return inflater.inflate(R.layout.fragment_training, container, false)
  }

  fun setup(phone: String)
  {
    val trening_list_view = activity?.findViewById<ListView>(R.id.trening_list)
    trainingListAdapter = TrainingListAdapter(
      activity,
      activity?.applicationContext as Context,
      R.layout.training_item_view,
      training_list,
      declared_training_list,
      phone
    )
    trening_list_view?.adapter = trainingListAdapter

    val callback1 = object : Storage.AsyncCallback
    {
      override fun success(response: Response)
      {
        val responseString = response.body!!.string()
        val jsonArray = JSONArray(responseString)
        declared_training_list.clear()
        for (i in 0 until jsonArray.length())
        {
          declared_training_list.add(jsonArray.getString(i))
        }
        activity?.runOnUiThread { trainingListAdapter?.notifyDataSetChanged() }
      }

      override fun failure()
      {
      }
    }
    val callback = object : Storage.AsyncCallback
    {
      override fun success(response: Response)
      {
        val responseString = response.body!!.string()
        val jsonArray = JSONArray(responseString)
        training_list.clear()
        for (i in 0 until jsonArray.length())
        {
          training_list.add(jsonArray.getString(i))
        }

        Storage.list_declared_trainings(phone, callback1)
      }

      override fun failure()
      {
      }
    }
    Storage.list_trainings(callback)
  }
}