package com.karate.fragment

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.fragment.app.Fragment
import com.karate.R
import com.karate.Storage
import okhttp3.Response
import org.json.JSONArray

class TrainingFragment : Fragment()
{
  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View
  {
    return inflater.inflate(R.layout.fragment_add_training, container, false)
  }

  override fun onStart()
  {
    super.onStart()

    val input_time_view = LayoutInflater.from(context).inflate(R.layout.time_layout, null)
    val timePicker = input_time_view.findViewById<TimePicker>(R.id.time_picker)
    timePicker.setIs24HourView(true)
    val input_date_view = LayoutInflater.from(context).inflate(R.layout.date_layout, null)
    val datePicker = input_date_view.findViewById<DatePicker>(R.id.date_picker)

    val time_dialog_builder = AlertDialog.Builder(activity)
      .setTitle("dodaj trening")
      .setView(input_time_view)
      .setNegativeButton("anuluj") { dialog, _ -> dialog.dismiss() }
      .setPositiveButton("dodaj") { dialog, _ ->
        dialog.cancel()
        val date = "%d-%d-%d".format(datePicker.year, datePicker.month + 1, datePicker.dayOfMonth)
        val time = "%d-%d".format(timePicker.currentHour, timePicker.currentMinute)
        Storage.add_training("$date-$time")
      }
      .create()

    val date_dialog_builder = AlertDialog.Builder(activity)
      .setTitle("dodaj trening")
      .setView(input_date_view)
      .setNegativeButton("anuluj") { dialog, _ -> dialog.dismiss() }
      .setPositiveButton("dalej") { dialog, _ ->
        dialog.cancel()
        time_dialog_builder.show()
      }
      .create()

    view?.findViewById<Button>(R.id.add_training)?.setOnClickListener {
      date_dialog_builder.show()
    }

    val remove_training_dialog_builder = AlertDialog.Builder(activity)
      .setTitle("usuń trening")
      .setPositiveButton("ok") { dialog, _ -> dialog.cancel() }

    view?.findViewById<Button>(R.id.remove_training)?.setOnClickListener {
      val callback = object : Storage.AsyncCallback
      {
        override fun success(response: Response)
        {
          val context = activity?.applicationContext as Context
          val adapter = ArrayAdapter<String>(context, android.R.layout.simple_list_item_1)

          val responseString = response.body!!.string()
          val jsonArray = JSONArray(responseString)
          for (i in 0 until jsonArray.length())
          {
            adapter.add(jsonArray.getString(i))
          }

          activity?.runOnUiThread {
            remove_training_dialog_builder
              .setAdapter(adapter) { _, i -> Storage.remove_training(adapter.getItem(i) as String) }
              .show()
          }
        }

        override fun failure()
        {
        }
      }
      Storage.list_trainings(callback)
    }
  }
}
