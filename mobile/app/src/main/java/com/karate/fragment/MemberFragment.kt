package com.karate.fragment

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputEditText
import com.karate.R
import com.karate.Storage
import okhttp3.Response
import org.json.JSONArray

class MemberFragment : Fragment()
{
  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View
  {
    return inflater.inflate(R.layout.fragment_member, container, false)
  }

  override fun onStart()
  {
    super.onStart()

    val input_view = LayoutInflater.from(context).inflate(R.layout.add_member_layout, null)
    val add_member_dialog_builder = AlertDialog.Builder(activity)
      .setTitle("dodaj uczestnika")
      .setView(input_view)
      .setPositiveButton("ok") { dialog, _: Int ->
        val name = input_view.findViewById<TextInputEditText>(R.id.input_name).text.toString()
        val phone = input_view.findViewById<EditText>(R.id.input_phone).text.toString()
        val level = input_view.findViewById<EditText>(R.id.input_level).text.toString()
        println(name)
        dialog?.cancel()
        Storage.add_member(name, phone, level)
      }
      .create()

    view?.findViewById<Button>(R.id.add_member)?.setOnClickListener {
      add_member_dialog_builder.show()
    }

    val remove_member_dialog_builder = AlertDialog.Builder(activity)
        .setTitle("usuń uczestnika")
        .setPositiveButton("ok") { dialog, _: Int -> dialog?.cancel() }

    view?.findViewById<Button>(R.id.remove_member)?.setOnClickListener {
      val callback = object : Storage.AsyncCallback
      {
        override fun success(response: Response)
        {
          val context = activity?.applicationContext as Context
          val adapter = ArrayAdapter<String>(context, android.R.layout.select_dialog_item)
          val phones = ArrayList<String>()

          val responseString = response.body!!.string()
          val jsonArray = JSONArray(responseString)
          for (i in 0 until jsonArray.length())
          {
            val json = jsonArray.getJSONObject(i)
            adapter.add(json.getString("name"))
            phones.add(json.getString("phone"))
          }

          activity?.runOnUiThread {
            remove_member_dialog_builder
              .setAdapter(adapter) { _, i -> Storage.remove_member(phones[i]) }
              .show()
          }
        }

        override fun failure()
        {
        }
      }
      Storage.list_members(callback)
    }

    val list_members_dialog_builder = AlertDialog.Builder(activity)
        .setTitle("zapisani uczestnicy")
        .setPositiveButton("ok") { dialog, _: Int -> dialog?.cancel() }

    view?.findViewById<Button>(R.id.list_members)?.setOnClickListener {
      val callback = object : Storage.AsyncCallback
      {
        override fun success(response: Response)
        {
          val context = activity?.applicationContext as Context
          val adapter = ArrayAdapter<String>(context, android.R.layout.simple_list_item_1)

          val responseString = response.body!!.string()
          val jsonArray = JSONArray(responseString)
          for (i in 0 until jsonArray.length())
          {
            adapter.add(jsonArray.getJSONObject(i).getString("name"))
          }

          activity?.runOnUiThread {
            list_members_dialog_builder
              .setAdapter(adapter) { _, _ -> }
              .show()
          }
        }

        override fun failure()
        {
        }
      }
      Storage.list_members(callback)
    }
  }
}
