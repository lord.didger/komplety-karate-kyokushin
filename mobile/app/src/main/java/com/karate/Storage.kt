package com.karate

import android.os.AsyncTask
import okhttp3.*
import java.net.URL

class Storage
{
  companion object
  {
    private val httpClient = OkHttpClient()
    private const val service_url = "helloworld-eie2b5uvvq-ew.a.run.app"

    fun authorize(phone: String, callback: AsyncCallback)
    {
      val url = URL("https://$service_url/member/$phone")
      val request = Request.Builder().url(url).build()
      AsyncJob(request, callback).execute()
    }

    fun add_member(name: String, phone: String, level: String)
    {
      if(name.isEmpty() or phone.isEmpty() or level.isEmpty())
      {
        return
      }
      else
      {
        val url = URL("https://$service_url/member")
        val formBody: RequestBody = FormBody.Builder()
          .add("name", name)
          .add("phone", phone)
          .add("level", level)
          .build()
        val request = Request.Builder().url(url).post(formBody).build()
        AsyncJob(request, null).execute()
      }
    }

    fun remove_member(phone: String)
    {
      val url = URL("https://$service_url/member/$phone")
      val request = Request.Builder().url(url).delete().build()
      AsyncJob(request, null).execute()
    }

    fun list_members(callback: AsyncCallback)
    {
      val url = URL("https://$service_url/member")
      val request = Request.Builder().url(url).build()
      AsyncJob(request, callback).execute()
    }

    fun add_training(datetime: String)
    {
      val url = URL("https://$service_url/trening")
      val formBody: RequestBody = FormBody.Builder()
        .add("datetime", datetime)
        .add("confirmed", "true")
        .build()
      val request = Request.Builder().url(url).post(formBody).build()
      AsyncJob(request, null).execute()
    }

    fun remove_training(datetime: String)
    {
      val url = URL("https://$service_url/trening/$datetime")
      val request = Request.Builder().url(url).delete().build()
      AsyncJob(request, null).execute()
    }

    fun list_trainings(callback: AsyncCallback)
    {
      val url = URL("https://$service_url/trening")
      val request = Request.Builder().url(url).build()
      AsyncJob(request, callback).execute()
    }

    fun list_declared_trainings(phone: String, callback: AsyncCallback)
    {
      val url = URL("https://$service_url/member/$phone/trening")
      val request = Request.Builder().url(url).build()
      AsyncJob(request, callback).execute()
    }

    fun list_declared(datetime: String, callback: AsyncCallback)
    {
      val url = URL("https://$service_url/trening/$datetime")
      val request = Request.Builder().url(url).build()
      AsyncJob(request, callback).execute()
    }

    fun declare_attendance(phone: String, datetime: String)
    {
      val url = URL("https://$service_url/member/$phone/trening/$datetime")
      val formBody: RequestBody = FormBody.Builder().build()
      val request = Request.Builder().url(url).post(formBody).build()
      AsyncJob(request, null).execute()
    }

    fun undeclare_attendance(phone: String, datetime: String)
    {
      val url = URL("https://$service_url/member/$phone/trening/$datetime")
      val request = Request.Builder().url(url).delete().build()
      AsyncJob(request, null).execute()
    }
  }

  internal class AsyncJob(private val request: Request, private val callback: AsyncCallback?) :
    AsyncTask<Void?, Void?, Void?>()
  {
    override fun doInBackground(vararg params: Void?): Void?
    {
      val response: Response = httpClient.newCall(request).execute()
      if (response.code == 200)
      {
        callback?.success(response)
      }
      else
      {
        callback?.failure()
      }
      return null
    }
  }

  interface AsyncCallback
  {
    fun success(response: Response)
    fun failure()
  }
}