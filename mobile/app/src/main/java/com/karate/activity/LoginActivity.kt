package com.karate.activity

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.karate.R
import com.karate.Storage
import okhttp3.Response

class LoginActivity : AppCompatActivity()
{
  override fun onCreate(savedInstanceState: Bundle?)
  {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.login_layout)
  }

  fun authorize(view: View)
  {
    val textView: TextView = findViewById(R.id.phone_number)
    val phone_number = textView.text.toString()

    if (phone_number.isEmpty())
    {
      AlertDialog.Builder(this)
        .setTitle("niepoprawne dane")
        .setMessage("Wstęp tylko dla członków klubu. Proszę podać numer telefonu.")
        .setPositiveButton(
          "rozumiem"
        ) { dialog: DialogInterface?, which: Int -> }
        .create()
        .show()
    }
    else
    {
      val callback = object : Storage.AsyncCallback
      {
        override fun success(response: Response)
        {
          val intent = Intent(this@LoginActivity, LoggedInActivity::class.java)
          val bundle = Bundle()
          bundle.putString(LoggedInActivity.phone_key, phone_number)
          intent.putExtras(bundle)
          startActivity(intent)
        }

        override fun failure()
        {
          this@LoginActivity.runOnUiThread {
            AlertDialog.Builder(this@LoginActivity)
              .setTitle("niepoprawne dane")
              .setMessage("Podany numer telefonu nie został potwierdzony.")
              .setPositiveButton(
                "przepraszam"
                ) { dialog: DialogInterface?, which: Int -> }
              .create()
              .show()
          }
        }
      }
      Storage.authorize(phone_number, callback)
    }
  }
}
