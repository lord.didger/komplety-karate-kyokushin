package com.karate.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.karate.R
import com.karate.fragment.ListTrainingsFragment

class LoggedInActivity : AppCompatActivity()
{
  companion object
  {
    const val phone_key = "phone_key"
  }

  override fun onCreate(savedInstanceState: Bundle?)
  {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.logged_in_layout)
    val bundle = intent.extras!!
    val phone = bundle.getString(phone_key)
    val fragment = supportFragmentManager.findFragmentById(R.id.training_list) as ListTrainingsFragment
    fragment.setup(phone as String)

    if(phone != "797665669" && phone != "508101717")
    {
      findViewById<View>(R.id.member_controls).visibility = View.GONE
      findViewById<View>(R.id.training_controls).visibility = View.GONE
    }
  }
}