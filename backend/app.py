from flask import abort
from flask import Flask
from flask import request
import json
import sqlalchemy

app = Flask(__name__)

db_config = {
    "pool_size": 5,
    "max_overflow": 2,
    "pool_timeout": 30,
    "pool_recycle": 1800
}

db_user = "karate"
db_pass = "karate"
db_name = "karate"
db_socket_dir = "/cloudsql"
cloud_sql_connection_name = "master-works-280721:europe-west3:database"

url = sqlalchemy.engine.url.URL(
    drivername="postgres+pg8000",
    username=db_user,
    password=db_pass,
    database=db_name,
    query={
        "unix_sock": "{}/{}/.s.PGSQL.5432".format(
            db_socket_dir,
            cloud_sql_connection_name)
    }
)
# db_hostname = "127.0.0.1"
# db_port = 5432
# url = sqlalchemy.engine.url.URL(
#     drivername="postgres+pg8000",
#     username=db_user,
#     password=db_pass,
#     database=db_name,
#     host=db_hostname,
#     port=db_port
# )

db = sqlalchemy.create_engine(url, **db_config)

def get_member_id(phone):
    with db.connect() as connection:
        clause = 'select id from member where phone = \'{}\''.format(phone)
        result = connection.execute(clause).fetchall()
        if len(result) > 0:
            return result[0]['id']
        else:
            return None

def get_trening_id(trening):
    with db.connect() as connection:
        clause = 'select id from trening where datetime = \'{}\''.format(trening)
        result = connection.execute(clause).fetchall()
        if len(result) > 0:
            return result[0]['id']
        else:
            return None

@app.route('/')
def hello_world():
    return 'Hello, World!'

def get_members():
    print('get_members')
    with db.connect() as connection:
        clause = 'select * from member'
        result = connection.execute(clause).fetchall()
        users = []
        for row in result:
            user = {
                'name' : row['name'],
                'phone' : row['phone'],
                'level' : row['level']
            }
            users.append(user)
        return json.dumps(users)

def add_member():
    if not all(field in request.form for field in ['name', 'level', 'phone']):
        abort(400)

    with db.connect() as connection:
        clause = 'insert into member (name, phone, level) values (\'{}\', \'{}\', {})'.format(
            request.form['name'],
            request.form['phone'],
            request.form['level']
        )
        result = connection.execute(clause)
        return "OK"

@app.route('/member', methods=['GET', 'POST'])
def member():
    if request.method == 'GET':
        return get_members()
    elif request.method == 'POST':
        return add_member()
    else:
        abort(500)

def get_member(phone):
    with db.connect() as connection:
        clause = 'select * from member where phone = \'{}\''.format(phone)
        result = connection.execute(clause).fetchall()
        if len(result) > 0:
            row = result[0]
            user = {
                'name' : row['name'],
                'phone' : row['phone'],
                'level' : row['level']
            }
            return json.dumps(user)
        else:
            abort(404)

def remove_member(phone):
    with db.connect() as connection:
        clause = 'delete from member where phone = \'{}\''.format(phone)
        result = connection.execute(clause)
        return "OK"

@app.route('/member/<phone>', methods=['GET', 'DELETE'])
def selected_member(phone):
    if request.method == 'GET':
        return get_member(phone)
    elif request.method == 'DELETE':
        return remove_member(phone)
    else:
        abort(500)

@app.route('/member/<phone>/trening', methods=['GET'])
def get_declared(phone):
    with db.connect() as connection:
        clause = '''select t.datetime from member as m
                    inner join declaration as d on m.id = d.member_id
                    inner join trening as t on t.id = d.trening_id
                    where m.phone = \'{}\'
                '''.format(phone)
        result = connection.execute(clause).fetchall()
        trenings = [row['datetime'] for row in result]
        return json.dumps(trenings)

@app.route('/member/<phone>/trening/<trening>', methods=['POST'])
def declare(phone, trening):
    member_id = get_member_id(phone)
    trening_id = get_trening_id(trening)
    if member_id is None or trening_id is None:
        abort(400)
    else:
        with db.connect() as connection:
            clause = 'insert into declaration ( member_id, trening_id ) values ( {}, {} )'.format(member_id, trening_id)
            result = connection.execute(clause)
            return "OK"

@app.route('/member/<phone>/trening/<trening>', methods=['DELETE'])
def undeclare(phone, trening):
    member_id = get_member_id(phone)
    trening_id = get_trening_id(trening)
    if member_id is None or trening_id is None:
        abort(400)
    else:
        with db.connect() as connection:
            clause = 'delete from declaration where member_id = {} and trening_id = {}'.format(member_id, trening_id)
            result = connection.execute(clause)
            return "OK"

def get_trenings():
    with db.connect() as connection:
        clause = 'select datetime, confirmed from trening'
        result = connection.execute(clause).fetchall()
        trenings = [row['datetime'] for row in result]
        return json.dumps(trenings)

def add_trening():
    if 'datetime' not in request.form:
        abort(400)

    with db.connect() as connection:
        clause = 'insert into trening (datetime, confirmed) values (\'{}\', {})'.format(
            request.form['datetime'],
            request.form['confirmed']
        )
        result = connection.execute(clause)
        return "OK"

@app.route('/trening', methods=['GET', 'POST'])
def trening():
    if request.method == 'GET':
        return get_trenings()
    elif request.method == 'POST':
        return add_trening()
    else:
        abort(500)

def get_trening(trening):
    with db.connect() as connection:
        clause = '''select m.name from member as m
                    inner join declaration as d on m.id = d.member_id
                    inner join trening as t on t.id = d.trening_id
                    where t.datetime = \'{}\'
                 '''.format(trening)
        result = connection.execute(clause).fetchall()
        members = [row['name'] for row in result]
        if len(result) > 0:
            return json.dumps(members)
        else:
            abort(404)

def remove_trening(trening):
    with db.connect() as connection:
        clause = 'delete from trening where datetime = \'{}\''.format(trening)
        result = connection.execute(clause)
        return "OK"

@app.route('/trening/<trening>', methods=['GET', 'DELETE'])
def selected_trening(trening):
    if request.method == 'GET':
        return get_trening(trening)
    elif request.method == 'DELETE':
        return remove_trening(trening)
    else:
        abort(500)
